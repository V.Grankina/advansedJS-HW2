import * as nodePath from 'path';
const rootFolder = nodePath.basename(nodePath.resolve());

const buildFolder = './dist';
const srcFolder = './src';

export const path = {
    build: {
        files:`${buildFolder}/files/`,
        html:`${buildFolder}/`,
        scss:`${buildFolder}/css/`,
        js:`${buildFolder}/js/`,
        images: `${buildFolder}/img/`,
    },
    src: {
        files: `${srcFolder}/files/**/*.*`,
        html:`${srcFolder}/index.html`,
        scss:`${srcFolder}/scss/style.scss`,
        js:`${srcFolder}/js/app.js`,
        images:`${srcFolder}/img/**/*.{jpeg,jpg,png,gif,webp}`,
        svg: `${srcFolder}/img/**/*.svg`,
    },
    watch: {
        files: `${srcFolder}/files/**/*.*`,
        html:`${srcFolder}/**/*.html`,
        scss:`${srcFolder}/**/*.scss`,
        js:`${srcFolder}/**/*.js`,
        images:`${srcFolder}/img/**/*.{jpeg,jpg,png,gif,webp,svg}`,
    },
    clean: buildFolder,
    buildFolder: buildFolder,
    srcFolder: srcFolder,
    rootFolder: rootFolder
}
